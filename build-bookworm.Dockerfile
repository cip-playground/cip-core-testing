# Copyright (c) Toshiba Corporation, 2024
#
# Authors:
#  Sai Ashrith <sai.sathujoda@toshiba-tsip.com>
#
# SPDX-License-Identifier: Apache-2.0

FROM debian:bookworm-slim

ARG DEBIAN_FRONTEND=noninteractive

# Install dependencies
RUN apt-get update \
&& apt-get install -y --no-install-recommends apt-utils lavacli curl unzip \
&& rm -rf /var/lib/apt/lists/*
