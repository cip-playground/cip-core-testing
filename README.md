# CIP Core functionality testing in LAVA

Functionalities like software update, secure boot and IEC layer in [isar-cip-core](https://gitlab.com/cip-project/cip-core/isar-cip-core) are being tested using LAVA in this project.

Job definitions present in this repository are being submitted to CIP LAVA server using a weekly scheduled pipeline. The test results are mailed to **cip-testing-results@lists.cip-project.org**.
