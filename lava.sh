#!/bin/bash
# Copyright (C) 2024, Renesas Electronics Europe GmbH
# Chris Paterson <chris.paterson2@renesas.com>
# Sai Ashrith <sai.sathujoda@toshiba-tsip.com>
################################################################################

set -e

################################################################################
LAVA_TEMPLATES="templates"
LAVA_USER=$1
LAVA_TOKEN=$2
LAVACLI_ARGS="--uri https://$LAVA_USER:$LAVA_TOKEN@${CIP_LAVA_LAB_SERVER:-lava.ciplatform.org}/RPC2"

# Create a dictionary to handle image arguments based on architecture
declare -A image_args
image_args[amd64]="-cpu qemu64 -machine q35,accel=tcg  -global ICH9-LPC.noreboot=off -device ide-hd,drive=disk -drive if=pflash,format=raw,unit=0,readonly=on,file=/usr/share/OVMF/OVMF_CODE_4M.secboot.fd -device virtio-net-pci,netdev=net -drive if=pflash,format=raw,readonly=on,file=/usr/share/OVMF/OVMF_VARS_4M.snakeoil.fd  -global ICH9-LPC.disable_s3=1 -global isa-fdc.driveA= -device tpm-tis,tpmdev=tpm0"
image_args[arm64]="-cpu cortex-a57 -machine virt -device virtio-serial-device -device virtconsole,chardev=con -chardev vc,id=con -device virtio-blk-device,drive=disk -device virtio-net-device,netdev=net -device tpm-tis-device,tpmdev=tpm0"
image_args[arm]="-cpu cortex-a15 -machine virt -device virtio-serial-device -device virtconsole,chardev=con -chardev vc,id=con -device virtio-blk-device,drive=disk -device virtio-net-device,netdev=net -device tpm-tis-device,tpmdev=tpm0"

set_up () {
	job_dir="$(mktemp -d)"
}

clean_up () {
	rm -rf "$job_dir"
}

# This method is called only for arm64 and arm targets
add_firmware_artifacts () {
	sed -i "s@#Firmware#@firmware:@g" $2
	sed -i "s@#Firmware_args#@image_arg: '-bios {firmware}'@g" $2
	sed -i "s@#Firmware_url#@url: https://s3.eu-central-1.amazonaws.com/download2.cip-project.org/cip-core/next/qemu-${1}/firmware.bin@g" $2
}

create_jobs () {
	if [ $1 = "IEC_Layer_test" ]; then
		for arch in amd64 arm64 arm
		do
			cp $LAVA_TEMPLATES/IEC_template.yml ${job_dir}/IEC_${arch}.yml
			sed -i "s@#architecture#@${arch}@g" ${job_dir}/IEC_${arch}.yml
			sed -i "s@#imageargs#@${image_args[$arch]}@g" ${job_dir}/IEC_${arch}.yml

			if [ $arch != amd64 ]; then
				add_firmware_artifacts $arch ${job_dir}/IEC_${arch}.yml
			fi
		done

	elif [ $1 = "software_update_test" ]; then
		if [ -z $2 ]; then
			 # Copying ustate check fail job definitions directly to job directory
			cp $LAVA_TEMPLATES/swupdate_ustate_verify_fail.yml ${job_dir}
			for arch in amd64 arm64 arm
			do
				cp $LAVA_TEMPLATES/swupdate_template.yml ${job_dir}/swupdate_${arch}.yml
				sed -i "s@#architecture#@${arch}@g" ${job_dir}/swupdate_${arch}.yml
				sed -i "s@#imageargs#@${image_args[$arch]}@g" ${job_dir}/swupdate_${arch}.yml

				if [ $arch != amd64 ]; then
					add_firmware_artifacts $arch ${job_dir}/swupdate_${arch}.yml
				fi
			done
		else
			cp $job_dir/swupdate_amd64.yml ${job_dir}/${2}.yml
			sed -i "s@software update testing@${2}@g" ${job_dir}/${2}.yml
			sed -i "s@) = 2@) = 0@g" ${job_dir}/${2}.yml
			if [ $2 = "kernel_panic" ]; then
				sed -i "s@next@maintain-lava-artifact@g" ${job_dir}/${2}.yml
				sed -i "s@kernel: C:BOOT1:linux.efi@Kernel panic - not syncing: sysrq triggered crash@g" ${job_dir}/${2}.yml
			else
				sed -i "s@kernel: C:BOOT1:linux.efi@Can't open verity rootfs - continuing will lead to a broken trust chain!@g" ${job_dir}/${2}.yml
				sed -i "s@echo software update is successful!!@dd if=/dev/urandom of=/dev/sdb5 bs=512 count=1@g" ${job_dir}/${2}.yml
			fi
		fi
	else
		for arch in amd64 arm64 arm
		do
			cp $LAVA_TEMPLATES/secureboot_template.yml ${job_dir}/secureboot_${arch}.yml
			sed -i "s@#architecture#@${arch}@g" ${job_dir}/secureboot_${arch}.yml
			sed -i "s@#imageargs#@${image_args[$arch]}@g" ${job_dir}/secureboot_${arch}.yml

			if [ $arch != amd64 ]; then
				add_firmware_artifacts $arch ${job_dir}/secureboot_${arch}.yml
			fi
		done
	fi
}

create_cip_core_jobs () {
	create_jobs IEC_Layer_test
	create_jobs software_update_test
	create_jobs secure_boot_test
	create_jobs software_update_test kernel_panic
	create_jobs software_update_test initramfs_crash
}

# $1: Job description yaml file
submit_job() {
        # Make sure yaml file exists
	if [ -f "$1" ]; then
		echo "Submitting $1 to LAVA master..."
		# Catch error that occurs if invalid yaml file is submitted
		local ret=$(lavacli $LAVACLI_ARGS jobs submit "$1") || error=true

		if [[ $ret != [0-9]* ]]
		then
			echo "Something went wrong with job submission. LAVA returned:"
			echo "${ret}"
		else
			echo "Job submitted successfully as #${ret}."
		fi
	fi
}

# $1: Device-type to search for
is_device_online () {
	local lavacli_output=${job_dir}/lavacli_output

	# Get list of all devices
	lavacli $LAVACLI_ARGS devices list > "$lavacli_output"

	# Count the number of online devices
	local count=$(grep "(${1})" "$lavacli_output" | grep -c "Good")
	echo "There are currently $count \"${1}\" devices online."

	if [ "$count" -gt 0 ]; then
		return 0
	fi
	return 1
}

# $1: LAVA jobs (eg: $TMP_DIR/*.yml)
submit_jobs () {
	local error=false

	for JOB in ${job_dir}/*.yml; do
		local device=$(grep device_type "$JOB" | cut -d ":" -f 2 | awk '{$1=$1};1')
		if is_device_online "$device"; then
			submit_job "$JOB"
		else
			echo "Refusing to submit test job as there are no suitable devices available."
			error=true
		fi
	done

	if $error; then
		return 1
	fi
	return 0
}

# This method is added with the intention to check if all the jobs are valid before submit
# If even a single definition is found to be invalid, then no job shall be submitted until
# it is fixed by the maintainer
validate_jobs () {
	local error=false

	for JOB in ${job_dir}/*.yml; do
		if $(lavacli $LAVACLI_ARGS jobs validate $JOB); then
			echo "$JOB is a valid definition"
		else
			echo "$JOB is not a valid definition"
			error=true
		fi
	done

	if $error; then
		return 1
	fi
	return 0
}

set_up
create_cip_core_jobs
validate_jobs
submit_jobs
clean_up
